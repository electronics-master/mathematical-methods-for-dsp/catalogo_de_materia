m = 8 % Number of vectores
n = 2 % Dimension for vectores

A = 1 / sqrt(m)
F = []
for k=1:m
  f_k = [];
  for j=0:(n-1)
    % f_k = vertcat(f_k, A * e^(2 * pi * i * j * (k - 1) / m));
    f_k = vertcat(f_k, e^(2 * pi * i * j * (k - 1) / m));
  endfor
  F = horzcat(F, f_k);
endfor

F'
v = [1; 3];

F' * v
