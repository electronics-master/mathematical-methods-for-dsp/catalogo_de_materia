function out = f(x)
  A = [2, 1; 1, 3];
  b = [4, 7]';
  
  out = 0.5 * x' * A * x - b' * x;
  % out = x(1)**2 + x(1) * x(2) + 1.5*x(2)**2 - 4*x(1) - 7 * x(2);
endfunction
