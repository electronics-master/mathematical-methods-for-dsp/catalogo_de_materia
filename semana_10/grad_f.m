function grad = grad_f(x)
  grad = [ 2 * x(1) + x(2) - 4;
	   x(1) + 3 * x(2) - 7;
	 ];
endfunction
