x = [0.5, 0.5]';

## The Following code was used to get the gradient
## syms x y z;
## 
## f = x**3 + y**3 + z**3  - 2*x*y - 2*x*z - 2*y*z;
##
## grad = gradient(f, [x, y, z])

g = grad_f(x);
d = -g;

while 1
  alpha = 1;
  delta = 0.5;
					     
  while (f(x + alpha * d) - f(x)) > delta * alpha * g' * d
    alpha = alpha / 2;
  endwhile

  old_x = x;
  x = x + alpha * d

  if norm(x - old_x) < 1e-5
    break
  endif

  old_g = g;
  g = grad_f(x);

  beta_k = norm(g) ** 2 / norm(old_g) ** 2;

  d = -g + beta_k  * d;
endwhile

% Resultado: [1, 2]
