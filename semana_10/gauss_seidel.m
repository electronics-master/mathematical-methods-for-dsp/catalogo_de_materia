x = 1;
y = 1;
z = 1;

old_vec = [x,y,z]

while 1
  x = fminsearch (@(x) (x**3 + y**3 + z**3  - 2*x*y - 2*x*z - 2*y*z), 0);
  y = fminsearch (@(y) (x**3 + y**3 + z**3  - 2*x*y - 2*x*z - 2*y*z), 0);
  z = fminsearch (@(z) (x**3 + y**3 + z**3  - 2*x*y - 2*x*z - 2*y*z), 0);

  vec = [x, y, z]

  if norm(vec - old_vec) < 1e-3
    break
  endif
  old_vec = vec;
endwhile

% Resultado: [1.333, 1.333, 1.333]
