function [x, r] = matching_pursuit(A, y, k)
  ## Entradas:
  ##   A: Matriz de medicion
  ##   y: Vector ruidoso de entrada
  ##   k: Esparcidad
  ## Parametros Iniciales:
  ##   r_0: Error recidual
  ##   x_0: Estimado inicial
  ## Salidas:
  ##   r: Error recidual
  ##   x: Vector estimado

  r = y;

  m = size(A)(2);

  x = zeros(m, 1);

  while 1
    g_i = A' * r;

    j = 0;
    max_j = -Inf;
    for l=1:m
      arg = abs(g_i(l)) / norm(A(:, l));
      if arg > max_j
	max_j = arg;
	j = l;
      endif
    endfor

    x(j) = x(j) + g_i(j) / norm(A(:, j)) ** 2;
    r = r - A(:, j) * g_i(j) / norm(A(:, j)) ** 2;

    if nnz(x) == k
      break;
    endif
  endwhile
endfunction
