A = [0.42, 0.08, 0.56, 0.62, 0.38, 0.26, 0.52, 0.86, 0.62, 0.92;
     0.49, 0.66, 0.71, 0.21, 0.10, 0.24, 0.41, 0.86, 0.78, 0.38;
     0.58, 0.05, 0.49, 0.65, 0.69, 0.62, 0.22, 0.28, 0.95, 0.16];

y = [0.47;
     0.76;
     0.165];

k = 2;

x1 = matching_pursuit(A, y, k);

x2 = orthogonal_matching_pursuit(A, y, k);

x3 = directional_pursuit(A, y, k);

x4 = iterative_hard_thresholding(A, y, k);
