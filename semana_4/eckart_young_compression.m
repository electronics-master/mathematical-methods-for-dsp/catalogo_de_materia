pkg load image;

input_image = imread("lena_850.png");
input_image = double(input_image);

rank = 700;

[left_eigenvectors, sigma, right_eigenvectors] = svds(input_image, rank);

compressed_image = left_eigenvectors*sigma*right_eigenvectors';

compressed_image = mat2gray(compressed_image);

imwrite(compressed_image, "compressed_700.png");
